from mitmproxy import http


class Cleaner:
    url_filter = "example.com"
    bad_words = ["use", "domain"]
    bad_url_params = []

    def response(self, flow: http.HTTPFlow) -> None:
        if self.url_filter in flow.request.pretty_url:
            fullpath = self.path + "/" + self.clean_url(flow.request.url)
            modified_response = flow.response
            for word in bad_words:
                modified_response = modified_response.replace(word, "*" * len(word))
            flow.response = http.HTTPResponse.make(200, modified_response)

    def request(self, flow: http.HTTPFlow) -> None:
        if self.url_filter in flow.request.pretty_url:
            fullpath = self.path + "/" + self.clean_url(flow.request.url)

    def clean_url(self, url):
        filename = url
        filename = filename.replace("https://", "")
        filename = filename.replace("?", "_")
        filename = filename.replace(",", "-")
        return filename


addons = [Cleaner()]
