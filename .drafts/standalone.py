#!/bin/env python
import asyncio
import sys
from mitmproxy import options
from mitmproxy.tools import dump

from custom_middleware import custom_middleware


class RequestLogger:
    def request(self, flow):
        print(flow.request)


async def start_proxy(host, port):
    opts = options.Options(listen_host=host, listen_port=port)

    master = dump.DumpMaster(
        opts,
        with_termlog=False,
        with_dumper=False,
    )
    # master.addons.add(RequestLogger())
    master.addons.add(HostToClientWordConcealer(master))

    await master.run()
    return master


if __name__ == "__main__":
    host = sys.argv[1]
    port = int(sys.argv[2])
    asyncio.run(start_proxy(host, port))
