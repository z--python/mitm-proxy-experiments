# redirect_to_localhost.py
from mitmproxy import ctx
from mitmproxy import http

REMOTE_HOST = "example.com"
DEV_HOST = "127.0.0.1"
DEV_PORT = 8000


def request(flow: http.HTTPFlow) -> None:
    if flow.request.pretty_host in [REMOTE_HOST, DEV_HOST]:
        ctx.log.info("=== request")
        ctx.log.info(str(flow.request.headers))
        ctx.log.info(f"content: {str(flow.request.content)}")

        flow.request.scheme = "http"
        flow.request.host = DEV_HOST
        flow.request.port = DEV_PORT


def response(flow: http.HTTPFlow) -> None:
    if flow.request.pretty_host == DEV_HOST:
        ctx.log.info("=== response")
        ctx.log.info(str(flow.response.headers))
        if flow.response.headers.get("Content-Type", "").startswith("image/"):
            return
        ctx.log.info(f"body: {str(flow.response.get_content())}")
