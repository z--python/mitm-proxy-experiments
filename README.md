# My first MITM proxy

## Installation

pip install -r ./requirements.txt

## Usage

1. Import the Locky middleware into the project
2. Start the MITM proxy

```
  python3 ./main.py
```

## Testing

```
  curl http://example.com --proxy http://127.0.0.1:8888 -k
```
