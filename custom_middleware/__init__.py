from http.client import HTTPResponse
from io import BytesIO
from mitm.core import Connection, Middleware
from . import *


class CustomMiddleware(Middleware):
    def __init__(self):
        self.connection: Connection = None

    async def mitm_started(self, host: str, port: int):
        pass

    async def client_connected(self, connection: Connection):
        pass

    async def server_connected(self, connection: Connection):
        pass

    async def client_data(self, connection: Connection, data: bytes) -> bytes:
        return data

    async def server_data(self, connection: Connection, data: bytes) -> bytes:
        return data

    async def client_disconnected(self, connection: Connection):
        pass

    async def server_disconnected(self, connection: Connection):
        pass
