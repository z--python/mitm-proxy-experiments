import email
import hashlib

from . import CustomMiddleware
from io import BytesIO
from mitm.core import Connection
from http.client import HTTPResponse
from os import makedirs, path
from tempfile import NamedTemporaryFile

CACHE_DIR = "/tmp/mitm"


class _FakeSocket:
    def __init__(self, response_bytes):
        self._file = BytesIO(response_bytes)

    def makefile(self, *args, **kwargs):
        return self._file


class ClientToHostGetParamsRemover(CustomMiddleware):
    """
    Middleware to remove some request get parameters.
    """

    async def mitm_started(self, host: str, port: int):
        print(f"[GetParamsRemover] MITM server started on {(f'{host}:{port}')}.")

    async def client_data(self, connection: Connection, data: bytes) -> bytes:
        if connection.server:
            print(f"Client {connection.client} to {connection.server}: \n\n\t{data}\n")
            return data
        print("-=-=-=-=-=-")
        return data


class HostToClientWordConcealer(CustomMiddleware):
    """
    Middleware to masks some response html.
    """

    BLACKLIST = ["domain"]

    def _http_response(headers, html, encoding_name="UTF-8"):
        return f"""HTTP/1.1 200 OK\r\n{
            headers
        }\r\n\r\n{
            html
        }""".encode(
            encoding_name
        )

    async def mitm_started(self, host: str, port: int):
        print(f"[WordConcealer] MITM server started on {(f'{host}:{port}')}.")

    async def server_data(self, connection: Connection, data: bytes) -> bytes:
        print(
            f"Server {connection.server} to client {connection.client}: \n\n\t{data}\n"
        )
        print("BLACKLIST", self.BLACKLIST)
        encoding_name = "UTF-8"

        headers, html = data.decode().split("\r\n\r\n")

        cache_dir_s2c = f"{CACHE_DIR}/serverToClient"
        if not path.exists(cache_dir_s2c):
            makedirs(cache_dir_s2c)

        # mask blacklisted words
        for bad_word in self.BLACKLIST:
            html = html.replace(bad_word, "*" * len(bad_word))

        print("cleaned up")
        print(html)

        tempfile_name = f"{cache_dir_s2c}/{hashlib.sha224(html.encode()).hexdigest()}"
        print(tempfile_name)
        if not path.isfile(tempfile_name):
            with open(tempfile_name, mode="w") as chunk:
                chunk.write(html)
                chunk.close()
        print("done")
        print(headers)
        print(html)
        xdata = self._http_response(headers, html, encoding_name)
        print(xdata)
        return xdata

        #
        #
        #
        #

        print(html)

        print(888)
        source = _FakeSocket(data)

        print(source)
        print(999)

        resp = HTTPResponse(source)

        print(resp)

        print(456)
        while resp.status == "UNKNOWN":
            time.sleep(1)
        print(resp.status)

        resp.begin()
        print(789)

        # encoding_name = resp.getheader("Content-Type").split("charset=")[1].upper()

        real_content = resp.read(len(data)).decode(encoding_name)
        # real_content = resp.read().decode(encoding_name)

        print(111)

        modified_content = real_content
        for bad_word in self.BLACKLIST:
            modified_content = modified_content.replace(bad_word, "*" * len(bad_word))

        modified_headers = []
        for header in resp.getheaders():
            (header_name, value) = header
            # print(header_name, value)
            # if header_name in blacklist:
            #     continue
            # if header_name == "Content-Length":
            #     value = str(len(modified_content))
            modified_headers.append(f"{header_name}: {value}")
        # print(modified_headers)

        modified_headers = "\r\n".join(modified_headers)
        modified_data = ""

        resp.close()

        print('--- "Fixed" response ---')
        print(modified_data)

        print("Changes made:", modified_content != real_content)

        return modified_data
