from mitm import MITM  # , protocol, crypto

# my middlewares
from custom_middleware.custom_middleware import (
    # ClientToHostGetParamsRemover,
    HostToClientWordConcealer,
)

HostToClientWordConcealer.BLACKLIST = ["use"]


if __name__ == "__main__":
    mitm = MITM(
        host="127.0.0.1",
        port=8888,
        middlewares=[
            # ClientToHostGetParamsRemover,
            HostToClientWordConcealer,
        ],
        # protocols=[protocol.HTTP],
        # certificate_authority=crypto.CertificateAuthority(),
    )
    mitm.run()
